#-------------------------------------------------
#
# Project created by QtCreator 2017-02-17T16:18:27
#
#-------------------------------------------------

QT       += core gui

TARGET = Designer
TEMPLATE = app


SOURCES += main.cpp \
    mydialogwindow.cpp

HEADERS  += \
    mydialogwindow.h

FORMS += \
    mydialogwindow.ui
